import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "PongServlet", urlPatterns = "/pong")
public class PongServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //     1.  Set response content type
        response.setContentType("text/html");

        //     2.   get the printWriter
        PrintWriter writer = response.getWriter();

        //      3.   generate the HTML content
        writer.println("<h2>Hello from Pong Servlet</h2> ");

        writer.println("<a href= \"/ping\" >Go to Ping Servlet</a>");

    }

}
