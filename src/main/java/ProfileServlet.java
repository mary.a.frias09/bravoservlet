import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "ProfileServlet", urlPatterns = "/profile")
public class ProfileServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession httpSession = request.getSession();

        boolean isValidUser = false;

        if(httpSession.getAttribute("user") != null) {

            isValidUser = (boolean) httpSession.getAttribute("user");
        }

        if (isValidUser) {

            request.setAttribute("username", httpSession);
            request.getRequestDispatcher("/profile.jsp").forward(request, response);

        } else {

            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }

    }
}
