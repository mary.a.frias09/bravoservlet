package CodeboundWishList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowProductsServlet", urlPatterns = "/products")
public class ShowProductsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //use the dao factory to get the dao object
        Products productsDao = ProductDaoFactory.getDao();

        //use a method on the dao to get all the products
        List<Product> products = productsDao.all();

        //pass the data to the jsp
        //two methods used

        request.setAttribute("listOfProducts", products);

        request.getRequestDispatcher("/product-app/show-my-products.jsp").forward(request, response);


        HttpSession httpSession = request.getSession();

        boolean isValidUser = false;

        if(httpSession.getAttribute("user") != null) {
            isValidUser = (boolean) httpSession.getAttribute("user");
        }

        if (isValidUser) {
            request.setAttribute("username", httpSession);
            request.getRequestDispatcher("/products").forward(request, response);

        } else {
            request.getRequestDispatcher("/login").forward(request, response);
        }



    }

}
