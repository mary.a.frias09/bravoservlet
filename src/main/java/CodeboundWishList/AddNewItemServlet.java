package CodeboundWishList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddNewItemServlet", urlPatterns = "/products/add-item")

public class AddNewItemServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//  use the dao factory to grt the dao object
        Products productsDao = ProductDaoFactory.getDao();

        String newName = request.getParameter("name");
        String newCategory = request.getParameter("category");
        double newPrice = Double.parseDouble("price");


        Product product = new Product(newName, newCategory, newPrice);

//        add product to our data base
        productsDao.insert(product);

        response.sendRedirect("/products");

    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher("/product-app/add-to-list.jsp").forward(request, response);

    }

}
