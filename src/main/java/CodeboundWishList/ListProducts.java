package CodeboundWishList;


import java.util.ArrayList;
import java.util.List;

// DAO Implementations
public class ListProducts implements Products {
//    create a field for the class
    private List<Product> products = new ArrayList<>();


    public ListProducts() {
        insert(new Product("Playstation 5", "Electronics", 499.99));
        insert(new Product("Nintendo Switch", "Electronics", 299.99));
        insert(new Product("Impact Driver", "Tool", 199.99));
        insert(new Product("65 Curved Tv", "Electronics", 995.99));
    }




    @Override
    public List<Product> all() {
        return this.products;
    }

    @Override
    public void insert(Product product) {
    this.products.add(product);
    }
}
