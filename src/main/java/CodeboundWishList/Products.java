package CodeboundWishList;

import java.util.List;

// DAO Interface
public interface Products {

    List<Product> all();   //gets all of the products or items
    void insert(Product product); //adds the new product to the data base
}
