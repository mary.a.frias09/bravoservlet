import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "TestExampleServlet", urlPatterns = "/exampleServlet")
public class TestExampleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //     1.  Set response content type
        response.setContentType("text/html");


        //  2.      get the PrintWriter
        PrintWriter printWriter = response.getWriter();

//          3.      generate the HTML content
        printWriter.println("<h1> Example Servlet</h1>");
        printWriter.println("<p>Created by me!</p>");
        printWriter.println("Time on the server is: " + new java.util.Date());
        printWriter.println("<br>");
        printWriter.println("<a href= \"/hello\" >Go to Hello Servlet</a>");
    }
}
