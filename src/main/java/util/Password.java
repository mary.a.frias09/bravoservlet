package util;

import org.mindrot.jbcrypt.BCrypt;

public class Password {
    public static void main(String[] args) {
//        create a string variable for our password
        String pw = "Password.123";
//
//        create a String variable for our hashing password
        String hash = BCrypt.hashpw(pw,BCrypt.gensalt());

//        BCrypt.hashpw(pw,BCrypt.gensalt());
//        generates a hash from the given plaintext password (pw)

//        System.out.println(hash);

//        outputs for hash will never be the same
//   OUTPUT     $2a$10$DIMC/65eExh96gtTV9aMZu7hb6ZluEubqfbNxtfVMGz9SVbp8BcPy
// 2ND OUTPUT   $2a$10$FjHwu/dKpuPrcth0W5huk.TIhuwuyJ0g3/PtSYFIwHMT58.MH4i8.

        boolean passwordsMatch = BCrypt.checkpw("mypassword", hash);
//        System.out.println(passwordsMatch);

         passwordsMatch = BCrypt.checkpw("Password.123", hash);
        System.out.println(passwordsMatch);

//      BCrypt.checkpw("Password.123", hash);
//        Verify that the given plaintext password matches a know hash
    }


}
