import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        boolean loginCred = username.equals("user") || username.equals("bravo") && password.equals("batman");

        if (loginCred) {
            request.getSession().setAttribute("user", true);
            request.getSession().setAttribute("username", username);
            response.sendRedirect("/profile");
        }   else {
            response.sendRedirect("/login");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession httpSession = request.getSession();

        boolean isValidUser = false;

        if(httpSession.getAttribute("user") != null) {
            isValidUser = (boolean) httpSession.getAttribute("user");
        }

        if (isValidUser) {
            request.getRequestDispatcher("/profile.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
    }
}
