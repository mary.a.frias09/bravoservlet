package StudentApp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddStudentServlet", urlPatterns = "/students/add-student")
public class AddStudentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
        Students studentsDao = DaoFactory.getStudentsDao();

//        enroll a new student, based on submitted data
        String newFirstName = request.getParameter("firstName");
        String newLastName = request.getParameter("lastName");
        String newEmail = request.getParameter("email");

        Student newStudent = new Student(newFirstName, newLastName, newEmail);

//        add the newStudent by using insert method from our Interface
        studentsDao.insert(newStudent);
        response.sendRedirect("/students");


    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/student-app/add-student.jsp").forward(request,response);
    }
}
