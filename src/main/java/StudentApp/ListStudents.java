package StudentApp;

import java.util.ArrayList;
import java.util.List;

// DAO IMPLEMENTATION
public class ListStudents implements Students {

//    INCORPORATING OUR DAO
    private List <Student> students = new ArrayList<>();

//   CREATE AN EMPTY LIST
//    when an instance of this class is created, we'll populate the students info "faking" our data
    public ListStudents() {
        insert(new Student("Leslie","Knope", "leslie@pawnee.com"));
        insert(new Student("Ron","Swanson", "ron@pawnee.com"));
        insert(new Student("Ben","Wyatt", "ben@pawnee.com"));

    }


    @Override
    public void insert(Student student) {
        this.students.add(student);
    }

    @Override
    public List<Student> all() {
        return this.students;
    }


}
