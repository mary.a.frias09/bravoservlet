package StudentApp;
// OUR MODEL


public class Student {
    // FIELDS


    private String firstName;
    private String lastName;
    private String email;


    // CONSTRUCTOR

    public Student(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }
    //GETTER AND SETTER

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
}


//    Data Interaction Classes
//        Model -> Student.java
//        DAO Interface -> Students.java
//        DAO Implementation -> ListStudents.java
//        Class to access DAO -> DaoFactory.java
//        View Students
//        Servlet/Controller - > ShowStudentsServlet.java
//        View -> jsp file
//        Creating/Enrolling Students-> Controller and View
