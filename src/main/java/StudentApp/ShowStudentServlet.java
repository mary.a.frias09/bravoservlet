package StudentApp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowStudentServlet", urlPatterns = "/students")
public class ShowStudentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        INCORPORATE OUR DAO

//        Use the factory to get the DAO object
        Students studentsDao = DaoFactory.getStudentsDao();

//        Use a method on the DAO to get all the students
        List<Student> students = studentsDao.all();

//         Pass the 'data' to the JSP
        request.setAttribute("listOfStudents", students);
        request.getRequestDispatcher("/student-app/students.jsp").forward(request,response);

    }
}
