package StudentApp;
// DAO Interface

import java.util.List;

public interface Students {

    List<Student> all();  //get all the students data
    void insert (Student student);  //add new student to data base
}

