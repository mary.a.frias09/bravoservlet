<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student13
  Date: 7/14/20
  Time: 10:12 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View wish list</title>
</head>
<body>

<h1>Welcome to my Wish List</h1>

<%-- JSTL forEach directive--%>


    <c:forEach items="${listOfProducts}" var="product">
        <h3>${product.name}</h3>
        <p>${product.category}</p>
        <p>$ ${product.price}</p>
    </c:forEach>

<a href="/products/add-item">Add to my WishList</a>


</body>
</html>
