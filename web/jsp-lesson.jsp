<%--
  Created by IntelliJ IDEA.
  User: student13
  Date: 7/10/20
  Time: 1:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title> JSP - LESSON </title>
</head>
<body>
<%@include file="WEB-INF/partials/navbar.jsp"%>

    <h3> JSP Expression </h3>
    <p> Converting a string to uppercase: <%= ("I'm not yelling!").toUpperCase()%> </p>
    <br>
    <p> 25 multiply by 4: <%= 25 * 4 %> </p>
    <br>
    <p> Is 75, less than 68? (True of False): <%= 75 < 68 %> </p>

    <h3>JSP Scriptlet</h3>
<%-- <%= SOME JAVA CODE%> --%>

<%--    <%--%>
<%--        for (int i = 1; i <= 5; i++) {--%>
<%--            out.println(i);--%>
<%--        }--%>
<%--    %>--%>


    <h3> JSP Declarations </h3>
    <%!
        String makeItLower(String data) {
            return data.toLowerCase();
        }
    %>

<%-- Call our makeItLower() method --%>
    <%=
        makeItLower("Hello BravO!")
    %>


<%-- EXPRESSION LANGUAGE (EL) --%>
    <h3> Expression Language (EL) </h3>
<%@include file="WEB-INF/partials/el.jsp"%>
<%-- ALTERNATIVE SYNTAX for include directive --%>
<%--<jsp:include page="WEB-INF/partials.el.jsp"></jsp:include>--%>
<%--<jsp:include page="WEB-INF/partials.el.jsp"/>--%>

    <p>JSP EL</p>
    <p>The JSP Expression Language, is more syntax that we can use in combining our JSP files</p>
    <p>El makes it easy to access attributes from the REQUEST OBJECT</p>
    <p> making it convenient way of accessing properties on objects</p>


</body>
</html>
