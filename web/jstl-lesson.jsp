<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student10
  Date: 7/10/20
  Time: 2:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>JSTL</title>
</head>
<body>
<%@include file="WEB-INF/partials/navbar.jsp"%>

<h1>JSTL Directives</h1>
<h3>otherwise, choose, when</h3>
<%--    <c:choose>--%>
<%--        <c:when test="${boolean_expression_1}">--%>
<%--            <p>display if expression 1 is true</p>--%>
<%--        </c:when>--%>
<%--        <c:when test="${boolean_expression_2}">--%>
<%--            <p>display if expression 2 is true</p>--%>
<%--        </c:when>--%>
<%--        <c:otherwise>--%>
<%--            <p>none of the above tests were true</p>--%>
<%--        </c:otherwise>--%>
<%--    </c:choose>--%>

<%--Choose example--%>
<c:set value="12" var="num">

    </c:set>
        <c:choose>
            <c:when test="${num % 2 == 0}">
                <c:out value="${num} is an even number">
                </c:out>
<%--        <p>Number is even</p>--%>
    </c:when>
        <c:otherwise>
            <c:out value="${num} is an odd number">
            </c:out>
        </c:otherwise>
</c:choose>

<h3>c:forEach</h3>
<%--    <c:forEach items="${collections}" var="element">--%>
<%--    &lt;%&ndash;    code &ndash;%&gt;--%>
<%--    </c:forEach>--%>

<c:forEach var="i" begin="1" end="5">

    Item <c:out value="${i}"/> <br>

</c:forEach>

<%
    request.setAttribute("numbers", new int[]{1, 2, 3, 4, 5});
%>
<ul>
    <c:forEach items="${numbers}" var="num">
        <li>${num}</li>
    </c:forEach>
</ul>

<h3>if tags</h3>
<p>- allows us to conditionally show a piece of HTML</p>
<c:if test="${boolean_expression}">
    <p>Boolean is true</p>
</c:if>


<c:if test="${isAdmin}">
    <h1>Welcome back Admin!</h1>
<%--    extra content, only viewable by Admin...    --%>
</c:if>









</body>
</html>
