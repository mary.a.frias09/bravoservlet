<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student13
  Date: 7/13/20
  Time: 8:58 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Login</title>
</head>
<body>

<%--Navbar included--%>
<%@include file="WEB-INF/partials/navbar.jsp"%>

    <h1>Please Login!</h1>



<%-- BOOTSTRAP FORM--%>
<div>
    <form action="/login" method="post">
        <div class="form-group col-6">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" aria-describedby="emailHelp">
        </div>
        <div class="form-group col-6">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>

        <button type="submit" class="btn btn-primary">Check out my profile!</button>
    </form>
</div>


<%--<form action="/login" method="post">--%>
<%--    <label>Username</label>--%>
<%--    <input type="text" id="username" placeholder="Username...">--%>
<%--&lt;%&ndash;    <br>&ndash;%&gt;--%>
<%--    <label>Password</label>--%>
<%--    <input type="text" id="password" placeholder="Password...">--%>
<%--    <br>--%>
<%--    <input type="submit" value="Log in">--%>
<%--</form>--%>




<c:choose>
    <c:when test="${param.username == 'admin' && param.password == 'password'}">
        <%
            response.sendRedirect("/profile.jsp");
        %>
    </c:when>
</c:choose>


<%--<%--%>
<%--    String user = request.getParameter("username");--%>
<%--    if (user.equals("admin")){--%>
<%--        response.sendRedirect("/WEB-INF/profile.jsp");--%>
<%--    } else {--%>
<%--        response.sendRedirect("/WEB-INF/login.jsp");--%>
<%--    }--%>
<%--%>--%>



    <%-- Footer Included--%>
    <%@include file="WEB-INF/partials/footer.jsp"%>
</body>
</html>
