<%--
  Created by IntelliJ IDEA.
  User: student13
  Date: 7/13/20
  Time: 8:59 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
    <%@include file="WEB-INF/partials/bootstrap.jsp"%>
</head>
<body>
<%--Navbar included--%>
<%@include file="WEB-INF/partials/navbar.jsp"%>
<%-- Header --%>
<div class="jumbotron text-center">
    <h1>Welcome to my Project Page!</h1>
</div>

<%--<h>You successfully logged in!</h1>--%>

<div class="container">
    <h1>You have successfully logged in!</h1>
    <div class="row">

        <div class="card col-3" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Counter Servlet</h5>
                <p class="card-text">Created a counter servlet that increases whenever visited.</p>
                <a href="/count" class="btn btn-primary">Go to project</a>
            </div>
        </div>
        <br>
        <div class="card col-3" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Ping Servlet</h5>
                <p class="card-text">Created a servlet that's link to another servlet</p>
                <a href="/ping" class="btn btn-primary">Go to project</a>
            </div>
        </div>
        <br>
        <div class="card col-3" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">WishList Project</h5>
                <p class="card-text">View my WishList Project</p>
                <a href="/products" class="btn btn-primary">Go to project</a>
            </div>
        </div>
        <br>
    </div>

</div>







<%--    --%>
<%--    --%>
<%--</head>--%>
<%--<body>--%>

<%--    &lt;%&ndash;Navbar included&ndash;%&gt;--%>
<%--    <%@include file="WEB-INF/partials/navbar.jsp"%>--%>
<%--    &lt;%&ndash; Header &ndash;%&gt;--%>
<%--    <div class="jumbotron text-center">--%>
<%--        <h1>Welcome!</h1>--%>
<%--    </div>--%>





<%--    --%>



<%-- Footer Included--%>
<%@include file="WEB-INF/partials/footer.jsp"%>
</body>
</html>
